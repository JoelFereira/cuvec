import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

import users from './modules/users/users';
import providers from './modules/providers/providers';
import materials from './modules/materials/materials';
import quotations from './modules/quotations/quotations';

const store =  new Vuex.Store({
    modules: {
        users: users,
        providers: providers,
        materials: materials,
        quotations: quotations
    },
    state: {
    },
    getters: {
    },
    actions: {
    },
    mutations: {
    }
})

export default store