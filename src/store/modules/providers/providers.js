import Axios from 'axios';

const baseURL = 'http://localhost:4000'

const providerModule = {
    state: {
        providers: [],
        providerMsg: {
            msg: ''
        }
    },
    actions: {
        async getProviders({ commit }) {
            await Axios.get(`${baseURL}/provider`).then(
                response => {
                    commit('SET_PROVIDERS', response.data)
                }
            ).catch(
                e => {
                    commit('SET_PROVIDER_MSG', e.response.data)
                }
            )
        },
        async createProvider({ commit, dispatch }, body) {
            await Axios.post(`${baseURL}/create/provider`, body).then(
                response => {
                    commit('SET_PROVIDER_MSG', response.data)
                    dispatch('getProviders')
                }
            ).catch(
                e => {
                    commit('SET_PROVIDER_MSG', e.response.data)
                }
            )
        },
        async updateProvider({ commit, dispatch }, body) {
            await Axios.put(`${baseURL}/update/provider`, body).then(
                response => {
                    commit('SET_PROVIDER_MSG', response.data)
                    dispatch('getProviders')
                }
            ).catch(
                e => {
                    commit('SET_PROVIDER_MSG', e.response.data)
                }
            )
        }
    },
    mutations: {
        SET_PROVIDERS(state, payload) {
            state.providers = payload
        },
        SET_PROVIDER_MSG(state, payload) {
            state.providerMsg = payload
        }
    }
}

export default providerModule