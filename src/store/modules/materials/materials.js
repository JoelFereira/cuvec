import Axios from 'axios';

const baseURL = 'http://localhost:4000'

const materialsModule = {
    state: {
        materials: [],
        activeMaterials: [],
        materialsMsg: {
            msg: ''
        }
    },
    actions: {
        async getMaterials({ commit }) {
            await Axios.get(`${baseURL}/materials`).then(
                response => {
                    commit('SET_MATERIALS', response.data)
                }
            ).catch(
                e => {
                    commit('SET_MATERIALS_MSG', e.response.data)
                }
            )
        },
        async getActiveMaterials({ commit }) {
            await Axios.get(`${baseURL}/active/materials`).then(
                response => {
                    commit('SET_ACTIVE_MATERIALS', response.data)
                }
            ).catch(
                e => {
                    commit('SET_MATERIALS_MSG', e.response.data)
                }
            )
        },
        async createMaterial({ commit, dispatch }, body) {
            await Axios.post(`${baseURL}/create/material`, body).then(
                response => {
                    commit('SET_MATERIALS_MSG', response.data)
                    dispatch('getMaterials')
                }
            ).catch(
                e => {
                    commit('SET_MATERIALS_MSG', e.response.data)
                }
            )
        },
        async updateMaterialStatus({ commit, dispatch }, body) {
            await Axios.put(`${baseURL}/update/material/status`, body).then(
                response => {
                    commit('SET_MATERIALS_MSG', response.data)
                    dispatch('getMaterials')
                }
            ).catch(
                e => {
                    commit('SET_MATERIALS_MSG', e.response.data)
                }
            )
        },
        async updateMaterial({ commit, dispatch }, body) {
            await Axios.put(`${baseURL}/update/material`, body).then(
                response => {
                    commit('SET_MATERIALS_MSG', response.data)
                    dispatch('getMaterials')
                }
            ).catch(
                e => {
                    commit('SET_MATERIALS_MSG', e.response.data)
                }
            )
        }
    },
    mutations: {
        SET_MATERIALS(state, payload) {
            state.materials = payload
        },
        SET_ACTIVE_MATERIALS(state, payload) {
            state.activeMaterials = payload
        },
        SET_MATERIALS_MSG(state, payload) {
            state.materialsMsg = payload
        }
    }
}

export default materialsModule