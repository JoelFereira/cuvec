import Axios from 'axios';

const baseURL = 'http://localhost:4000'

const quotationsModule = {
    state: {
        quotations: [],
        clients: [],
        quotationsMsg: {
            msg: ''
        }
    },
    actions: {
        async getQuotations({ commit }) {
            await Axios.get(`${baseURL}/quotations`).then(
                response => {
                    commit('SET_QUOTATIONS', response.data)
                }
            ).catch(
                e => {
                    commit('SET_QUOTATIONS_MSG', e.response.data)
                }
            )
        },
        async getClients({ commit }) {
            await Axios.get(`${baseURL}/clients`).then(
                response => {
                    commit('SET_CLIENTS', response.data)
                }
            ).catch(
                e => {
                    commit('SET_QUOTATIONS_MSG', e.response.data)
                }
            )
        },
        async createQuotation({ commit, dispatch }, body) {
            await Axios.post(`${baseURL}/create/quotation`, body).then(
                response => {
                    commit('SET_QUOTATIONS_MSG', response.data)
                    dispatch('getQuotations')
                }
            ).catch(
                e => {
                    commit('SET_QUOTATIONS_MSG', e.response.data)
                }
            )
        },
        async updateQuotation({ commit }, body) {
            await Axios.put(`${baseURL}/update/quotation`, body).then(
                response => {
                    commit('SET_QUOTATIONS_MSG', response.data)
                }
            ).catch(
                e => {
                    commit('SET_QUOTATIONS_MSG', e.response.data)
                }
            )
        }
    },
    mutations: {
        SET_QUOTATIONS(state, payload) {
            state.quotations = payload
        },
        SET_CLIENTS(state, payload) {
            state.clients = payload
        },
        SET_QUOTATIONS_MSG(state, payload) {
            state.quotationsMsg.msg = payload
        }
    }
}

export default quotationsModule