import Axios from 'axios';

const baseURL = 'http://localhost:4000'

const usersModule = {
    state: {
        loginResult: null,
        session: false
    },
    actions: {
        async login({ commit }, body) {
            await Axios.post(`${baseURL}/login`, body).then(
                response => {
                    commit('SET_LOGIN_RESULT', response.data)
                }
            ).catch(
                e => {
                    commit('SET_LOGIN_RESULT', e.response.data)
                }
            )
        },
        async setSession({ commit }, data) {
            commit('SET_SESSION', true)
            if (data.keepOpen) {
                window.$cookies.set('Session', `${data.user}/${data.pass}`, Infinity)
            }
        },
        async closeSession({ commit }) {
            commit('SET_SESSION', false)
            if (window.$cookies.isKey("Session")) {
                window.$cookies.remove('Session')
            }
        }
    },
    mutations: {
        SET_LOGIN_RESULT(state, payload) {
            state.loginResult = payload
        },
        SET_SESSION(state, payload) {
            state.session = payload
        }
    }
}

export default usersModule