// Setting Plugin
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Importing Templates

const login = () => import(`./../pages/login.vue`)
const list = () => import(`./../pages/list.vue`)
const admin = () => import(`./../pages/admin.vue`)

// Setting Routes

const routes = [
  {
    path: '/',
    name: 'list',
    component: list,
    meta: {
      navbar: true,
      title: 'Lista de Cotizaciones',
      icon: 'fa-list-alt'
    }
  },
  {
    path: '/login',
    name: 'login',
    component: login,
    meta: {
      navbar: false,
      title: 'Login',
      icon: ''
    },
  },
  {
    path: '/admin',
    name: 'admin',
    component: admin,
    meta: {
      navbar: true,
      title: 'Administración',
      icon: 'fa-book-open'
    }
  }
]


// Setting VueRouter
const router = new VueRouter({
  routes // short for `routes: routes`
})

// Exporting Routes -> src/main.js
export default router